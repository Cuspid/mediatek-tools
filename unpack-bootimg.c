/* tools/mkbootimg/mkbootimg.c
**
** Copyright 2007, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/limits.h>
#include <math.h>

#include "bootimg.h"

int usage(void)
{
    fprintf(stderr, "usage: unpack-bootimg <filename>\n");
    return 1;
}

void extract(FILE *src, FILE *dest, size_t size)
{
    char buf[4096];
    size_t size_total = size;
    size_t i;
    while(size_total > 0)
    {
        i = fread(&buf, 1, fmin(sizeof(buf), size_total), src);
        if (i < 1) return;
        fwrite(&buf, 1, i, dest);
        size_total -= i;
    }
}

int main(int argc, char **argv)
{
    boot_img_hdr hdr;
    FILE *fh;
    FILE *fh_dest;
    char outfile[PATH_MAX];
    
    char *filename = argv[1];

    if (argc != 2)
    {
        return usage();
    }

    if (access(filename, F_OK) == -1)
    {
        fprintf(stderr, "Error: %s\n", strerror(errno));
        return usage();
    }

    fh = fopen(filename, "rb");
    if (fh < 0)
    {
        fprintf(stderr, "Error: %s\n", strerror(errno));
        return 1;
    }
    
    fread(&hdr, sizeof(hdr), 1, fh);
    
    printf("Kernel offset:     0x%x\n", hdr.kernel_addr);
    printf("Kernel size:       0x%x\n", hdr.kernel_size);
    printf("Ramdisk offset:    0x%x\n", hdr.ramdisk_addr);
    printf("Ramdisk size:      0x%x\n", hdr.ramdisk_size);
    printf("Second offset:     0x%x\n", hdr.second_addr);
    printf("Second size:       0x%x\n", hdr.second_size);
    printf("Tags offset:       0x%x\n", hdr.tags_addr);
    printf("Page size:         0x%x (%d)\n", hdr.page_size, hdr.page_size);
    printf("Board name:        %s\n", hdr.name);
    printf("Command line:      %s\n", hdr.cmdline);
    printf("Base offset:       0x%x\n", hdr.kernel_addr-0x00008000);

    printf("===============\nRepack command:\n");
    printf("# mkbootimg --kernel '%s-kernel' --ramdisk '%s-ramdisk.cpio' --cmdline '%s' --base 0x%x --board '%s' --pagesize %d -o '%s'\n",
            filename, filename, hdr.cmdline, (hdr.kernel_addr-0x00008000), hdr.name, hdr.page_size, filename);

    size_t page_size = hdr.page_size;
    size_t kernel_seek = ceil((float)sizeof(hdr) / page_size) * page_size;
    size_t ramdisk_seek = kernel_seek + ceil((float)hdr.kernel_size / page_size) * page_size;
    size_t second_seek = ramdisk_seek + ceil((float)hdr.ramdisk_size / page_size) * page_size;
    
    printf("kernel_seek: %d\n", kernel_seek);
    printf("Ramdisk addr: %d\n", ramdisk_seek);
    printf("total: %d\n", ramdisk_seek+hdr.ramdisk_size-kernel_seek);
    
    if (hdr.kernel_size > 0)
    {
        sprintf(outfile, "%s-kernel", filename);
        printf("Extract kernel: %s\n", outfile);
        fh_dest = fopen(outfile, "wb");
        if (fh_dest < 0)
        {
            fprintf(stderr, "Error: %s\n", strerror(errno));
            return 1;
        }
        fseek(fh, kernel_seek, SEEK_SET);
        fseek(fh, PART_HEADER_SIZE, SEEK_CUR); // skip header
        extract(fh, fh_dest, hdr.kernel_size-PART_HEADER_SIZE);
        fclose(fh_dest);
    }

    if (hdr.ramdisk_size > 0)
    {
        sprintf(outfile, "%s-ramdisk.cpio", filename);
        printf("Extract kernel: %s\n", outfile);
        fh_dest = fopen(outfile, "wb");
        if (fh_dest < 0)
        {
            fprintf(stderr, "Error: %s\n", strerror(errno));
            return 1;
        }
        fseek(fh, ramdisk_seek, SEEK_SET);
        fseek(fh, PART_HEADER_SIZE, SEEK_CUR); // skip header
        extract(fh, fh_dest, hdr.ramdisk_size-PART_HEADER_SIZE);
        fclose(fh_dest);
    }
    
    if (hdr.second_size > 0)
    {
        sprintf(outfile, "%s-second.cpio", filename);
        printf("Extract kernel: %s\n", outfile);
        fh_dest = fopen(outfile, "wb");
        if (fh_dest < 0)
        {
            fprintf(stderr, "Error: %s\n", strerror(errno));
            return 1;
        }
        fseek(fh, second_seek, SEEK_SET);
        fseek(fh, PART_HEADER_SIZE, SEEK_CUR); // skip header
        extract(fh, fh_dest, hdr.second_size-PART_HEADER_SIZE);
        fclose(fh_dest);
    }

    fclose(fh);
    
    return 0;
}
