/* tools/mkbootimg/mkbootimg.c
**
** Copyright 2007, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "bootimg.h"

int usage(void)
{
    fprintf(stderr, "usage: infobootimg <filename>\n");
    return 1;
}

int main(int argc, char **argv)
{
    boot_img_hdr hdr;
    FILE *fd;
    
    char *filename = argv[1];

    if (argc != 2)
    {
        return usage();
    }

    if (access(filename, F_OK) == -1)
    {
        fprintf(stderr, "error: file not acceptable\n", filename);
        return usage();
    }

    fd = fopen(filename, "r");
    if (fd < 0)
    {
        fprintf(stderr,"error: open fail failed\n");
        return 1;
    }

    fread(&hdr, sizeof(hdr), 1, fd);
     
    fclose(fd);
    
    printf("Kernel offset:     0x%x\n", hdr.kernel_addr);
    printf("Kernel size:       0x%x\n", hdr.kernel_size);
    printf("Ramdisk offset:    0x%x\n", hdr.ramdisk_addr);
    printf("Ramdisk size:      0x%x\n", hdr.ramdisk_size);
    printf("Second offset:     0x%x\n", hdr.second_addr);
    printf("Second size:       0x%x\n", hdr.second_size);
    printf("Tags offset:       0x%x\n", hdr.tags_addr);
    printf("Page size:         0x%x (%d)\n", hdr.page_size, hdr.page_size);
    printf("Board name:        %s\n", hdr.name);
    printf("Command line:      %s\n", hdr.cmdline);
    printf("Base offset:       0x%x\n", hdr.kernel_addr-0x00008000);
    
    return 0;
}
