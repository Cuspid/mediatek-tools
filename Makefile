all: mkbootimg infobootimg unpack-bootimg

mkbootimg: mkbootimg.c bootimg.h
	gcc -o mkbootimg -lcrypto mkbootimg.c

imginfo: infobootimg.c bootimg.h
	gcc -o infobootimg infobootimg.c

unpack-bootimg: unpack-bootimg.c bootimg.h
	gcc -o unpack-bootimg unpack-bootimg.c -lm

install:
	install -t "$(DESTDIR)/usr/bin" \
	infobootimg \
	mkbootimg \
	unpack-bootimg \
	mtpack

clean:
	rm -f infobootimg mkbootimg unpack-bootimg

